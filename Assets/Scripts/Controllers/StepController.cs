﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StepController : MonoBehaviour
{

    private int step;
    int nbHouses;
    private GameObject[] housesObjects;
    private List<House> sequence;
    private int cptClick;
    private String nestedId;

    public AudioSource audioSource;
    public AudioClip success;
    public AudioClip error;

    public GameObject healthBar;
    public GameObject gameOverMenu;
    public GameObject desktopGameOver;
    public GameObject desktopLayout;

    protected bool LOOSED = false;

    void Start()
    {
        cptClick = 0;
        step = 0;
        nbHouses = 5;
        housesObjects = GameObject.FindGameObjectsWithTag("House");
        sequence = new List<House>();

        step++;
        this.GenerateNewStep();

        gameOverMenu.SetActive(false);
        desktopGameOver.SetActive(false);
    }

    private void GenerateNewStep()
    {
        // Generate random house index
        int randomIndex = UnityEngine.Random.Range(0, this.housesObjects.Length);

        // Get random house
        GameObject newHouse = this.housesObjects[randomIndex];
        // Add it to sequence
        sequence.Add(newHouse.GetComponent<House>());

        StartCoroutine("PlayHouseSequence");
    }

    public void TockDoor(String id)
    {
        this.nestedId = sequence[this.cptClick].getId();

        if (id == nestedId)
        {

            healthBar.GetComponent<Healthbar>().GetSomeCandies(1);

            if (this.cptClick < this.sequence.Count - 1)
            { 
                print("Its OK");
                this.cptClick++;

                audioSource.PlayOneShot(success);
            }
            else
            {
                step++;
                this.cptClick = 0;
                print("Win this sequence");

                audioSource.PlayOneShot(success);

                this.GenerateNewStep();
            }
        }
        else
        {
            nestedId = null;
            sequence.Clear();
            step = 0;
            print("Lose");

            LoseScreens();
        }
    }

    public void ReturnMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void LoseScreens()
    {

        if (LOOSED)
        {
            return;
        }

        LOOSED = true;

        gameOverMenu.SetActive(true);
        desktopGameOver.SetActive(true);
        desktopLayout.SetActive(false);

        audioSource.PlayOneShot(error);
    }

    public bool GetLoseState()
    {
        return LOOSED;
    }

    IEnumerator PlayHouseSequence()
    {
        yield return new WaitForSeconds(0.3f);
        desktopLayout.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < sequence.Count; i++)
        {
            House h = sequence[i];
            h.Highlight();
            yield return new WaitForSeconds(0.7f);
            h.UnHighlight();
            yield return new WaitForSeconds(0.5f);    
        }
        yield return new WaitForSeconds(0.3f);  
        desktopLayout.SetActive(false);
    }
}
