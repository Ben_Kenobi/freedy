﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Healthbar : MonoBehaviour {

    public float CurrentCandies { get; set; }
    public float MaxCandies { get; set; }
    public float SuperCandies { get; set; }

    private float timeToLoseCandies = 3;
    private int tempsInt;
    private float time;
    private bool isPaused;
    public Slider healthbar;
    public GameObject stepManagerRef;

	// Use this for initialization
	void Start () {

        isPaused = false;
        MaxCandies = 21;

        //Resets health to full on game load
        CurrentCandies = 11f;

        //Reset number of supercandies
        SuperCandies = 0;

        healthbar.value = CalculateHealth();

        time = timeToLoseCandies;
    }
	
	// Update is called once per frame
	void Update () {

        /*

        tempsInt = Mathf.RoundToInt(time);

        if(time >= 0) {
            time -= Time.deltaTime;
        } else {
            LoseSomeCandies(1);
            time = timeToLoseCandies;
        }

        if (Input.GetKeyDown(KeyCode.X)) {
            GetSomeCandies(1);
        }
         */
	}


    public void GetSomeCandies(float newCandies)
    {
        //Add the new candy
        CurrentCandies += newCandies;
        healthbar.value = CalculateHealth();

        //If the character get full candies, he can add a super candies !
        if (CurrentCandies >= MaxCandies)
        {
            CurrentCandies = MaxCandies;
        }
    }

    public void LoseSomeCandies(float candiesLost)
    {

        if (stepManagerRef.GetComponent<StepController>().GetLoseState() == true)
        {
            return;
        }

        CurrentCandies = CurrentCandies - candiesLost;
        healthbar.value = CalculateHealth();


        if(CurrentCandies == 0f)
        {
            CurrentCandies = 0f;
            Debug.Log("no more candies");
            stepManagerRef.GetComponent<StepController>().LoseScreens();
        }
    }


    float CalculateHealth()
    {
        return CurrentCandies / MaxCandies;
    }

}
