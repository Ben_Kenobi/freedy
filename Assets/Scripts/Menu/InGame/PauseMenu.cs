﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

    public GameObject menuObject;
    public GameObject menuBackground;
    public AudioSource audioSource;
    public AudioClip mainSong;

    public KeyCode menuBtn;
    private bool isActive = false;
    private bool soundIsActive = true;

    private void Start()
    {
        menuObject.SetActive(false);
        menuBackground.SetActive(false);

        //Restarting time
        Time.timeScale = 1;

        audioSource.volume = 0.5f;
        audioSource.PlayOneShot(mainSong);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(menuBtn))
        {
            TogglePauseMenu();
        }
    }

    public void TogglePauseMenu()
    {
        isActive = !isActive;

        //menu is visible
        if (isActive)
        {
            menuObject.SetActive(true);
            menuBackground.SetActive(true);

            //Stop time
            Time.timeScale = 0;
        }
        else
        {
            menuObject.SetActive(false);
            menuBackground.SetActive(false);

            //Restarting time
            Time.timeScale = 1;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ToggleSound()
    {
        audioSource.mute = !audioSource.mute;        
    }
}
