﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerController : MonoBehaviour
{

    private string moveInputAxis = "Vertical";
    private string moveLateralInputAxis = "Horizontal";
    
    public float moveSpeed = 20;

    public GameObject stepManager;
    public GameObject ringText;

    private Rigidbody rb;

    //selection system
    public float rayLength;
    public LayerMask doors;
    private bool canTok = true;

    private bool canSelectHouse;
    public KeyCode selectBtn;

    public GameObject camera;



    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        canSelectHouse = false;

        ringText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();

        RaycastHit hit;

        //il n'y a pas de get once ! Du coup je chope un get et je n'execute qu'une seule fois le raycast pour pouvoir valider ou non
        if (OVRInput.Get(OVRInput.Button.One) || OVRInput.Get(OVRInput.Button.Two) || OVRInput.Get(OVRInput.Button.Three) || OVRInput.Get(OVRInput.Button.Four) || OVRInput.Get(OVRInput.RawButton.LIndexTrigger) || OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0.3f)
        {

            if (canTok == false)
            {
                return;
            }

            if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, rayLength, doors))
            {
                Debug.DrawRay(camera.transform.position, camera.transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            
                House h = hit.collider.transform.parent.gameObject.GetComponent<House>();
                String id = h.getId();
                StepController controller = stepManager.GetComponent<StepController>();

                controller.TockDoor(id);

                canTok = false;
            }
        }
        else
        {
            canTok = true;
        }

        // Does the ray intersect doors layered objects
        if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, rayLength, doors))
        {
            Debug.DrawRay(camera.transform.position, camera.transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            ringText.SetActive(true);
        }
        else
        {
            Debug.DrawRay(camera.transform.position, camera.transform.TransformDirection(Vector3.forward) * rayLength, Color.blue);
            ringText.SetActive(false);
        }

    }

    private void FixedUpdate()
    {
        OVRInput.FixedUpdate();

        float moveAxis = Input.GetAxis(moveInputAxis);
        float moveLateralAxis = Input.GetAxis(moveLateralInputAxis);

        Move(-moveAxis, moveLateralAxis);
    }

    private void Move(float input, float lateralInput)
    {
        //tranlate
        Vector3 currentForwardVector = camera.transform.forward;
        Vector3 lateralVector = camera.transform.right;

        lateralVector.y = 0;
        currentForwardVector.y = 0;

        transform.Translate(-currentForwardVector.normalized * input * moveSpeed);
        transform.Translate(lateralVector.normalized * lateralInput * moveSpeed);

    }
}
