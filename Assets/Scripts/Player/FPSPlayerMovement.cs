﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEditor.Animations;

public class FPSPlayerMovement : MonoBehaviour {

    public float speed = 6.0f;

    private CharacterController charCont;

    public GameObject stepManager;
    public GameObject ringText;

    private Animator doorAnimator;

    //selection system
    public float rayLength = 4.0f;
    public LayerMask doors;
    private bool canTok;

    public Camera camera;
   
    // Use this for initialization
    void Start () {
        charCont = GetComponent<CharacterController>();
        canTok = true;

        ringText.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;

        Vector3 movement = new Vector3(deltaX, 0, deltaZ);

        movement = Vector3.ClampMagnitude(movement, speed);

        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        charCont.Move(movement);

        RaycastHit hit;
       

        // Does the ray intersect doors layered objects
        if (Physics.Raycast(camera.transform.position, camera.transform.TransformDirection(Vector3.forward), out hit, rayLength, doors))
        {
            Debug.DrawRay(camera.transform.position, camera.transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            ringText.SetActive(true);

            doorAnimator = hit.collider.transform.gameObject.GetComponent<House>().GetComponentInChildren<Animator>();

            doorAnimator.SetBool("on_enter", true);


            if (Input.GetMouseButtonDown(0))
            {

                if (!canTok)
                    return;
         
                House house = hit.collider.transform.gameObject.GetComponent<House>();
                String id = house.getId();
                StepController controller = stepManager.GetComponent<StepController>();
                controller.TockDoor(id);

                doorAnimator.SetBool("on_click", true);

                canTok = false;
                
            }
            else
            {
                canTok = true;
            }

        }
        else
        {
            Debug.DrawRay(camera.transform.position, camera.transform.TransformDirection(Vector3.forward) * rayLength, Color.blue);
            ringText.SetActive(false);

            if(doorAnimator) {
                doorAnimator.SetBool("on_enter", false);
                doorAnimator.SetBool("on_click", false);
                doorAnimator = null;
            }

        }

    }
}
