﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCamera : MonoBehaviour {

    public enum RotationAxis
    {
        MouseX = 1,
        MouseY = 2
    }

    public RotationAxis axes = RotationAxis.MouseX;

    public float minimumVert = -45.0f;
    public float maximumVert = 45.0f;

    public float sensHorizontal = 10.0f;
    public float sensVertical = 10.0f;

    public float rotationX = 0;

    private Inertia inertiaX;
    private Inertia inertiaY;


    void Start()
    {
        inertiaX = new Inertia(-sensHorizontal, sensHorizontal, 0.9f, 0.3f, 0f);
        inertiaY = new Inertia(minimumVert, maximumVert, 0.9f, 0.3f, 0f);
    }
	
	// Update is called once per frame
	void Update () {

        

		if(axes == RotationAxis.MouseX)
        {
            float rotationX = inertiaX.update(Input.GetAxis("Mouse X") * sensHorizontal);
            transform.Rotate(0, rotationX, 0);
        }

        if (axes == RotationAxis.MouseY)
        {
            rotationX -= Input.GetAxis("Mouse Y") * sensVertical;
            rotationX = Mathf.Clamp(rotationX, minimumVert, maximumVert);
            rotationX = inertiaY.update(rotationX);

            float rotationY = transform.localEulerAngles.y;
            transform.localEulerAngles = new Vector3(rotationX, rotationY, 0);
        }
	}
}
