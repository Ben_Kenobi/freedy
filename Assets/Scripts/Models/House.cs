﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class House : MonoBehaviour {

    private String id;
    public Image layoutImage;
    public Sprite referenceImage;
    public Sprite referenceImageHilighted;

    void Start() {
        this.id = System.Guid.NewGuid().ToString();
        print(layoutImage);
        layoutImage = layoutImage.GetComponent<Image>();
        layoutImage.sprite = referenceImage;
    }

    public void Highlight(){
        layoutImage.sprite = referenceImageHilighted;
    }

    public void UnHighlight() {
        layoutImage.sprite = referenceImage;
    }
    public String getId() {
        return this.id;
    }
}
