﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HouseManager : MonoBehaviour {

    public Button House1, House2, House3, House4;


    // Use this for initialization
    void Start () {

        House1.onClick.AddListener(() => HouseClicked(1));
        House2.onClick.AddListener(() => HouseClicked(2));
        House3.onClick.AddListener(() => HouseClicked(3));
        House4.onClick.AddListener(() => HouseClicked(4));
    }

    void TaskOnClick()
    {
        //Output this to console when Button1 or Button3 is clicked
        Debug.Log("You have clicked the button!");
    }

    void HouseClicked(int buttonNo)
    {
        Debug.Log("Button clicked = " + buttonNo);
    }
}
